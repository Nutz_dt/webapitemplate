﻿using Schedule.Data.Abstract;
using Scheduler.Model.Entities;

namespace Schedule.Data.Repositories
{
    public class ScheduleRepository : EntityBaseRepository<Scheduler.Model.Entities.Schedule>, IScheduleRepository
    {
        public ScheduleRepository(SchedulerContext context)
            : base(context)
        { }
    }
}
