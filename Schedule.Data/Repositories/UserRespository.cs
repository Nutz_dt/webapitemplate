﻿using Schedule.Data.Abstract;
using Scheduler.Model.Entities;

namespace Schedule.Data.Repositories
{
    public class UserRepository : EntityBaseRepository<User>, IUserRepository
    {
        public UserRepository(SchedulerContext context)
            : base(context)
        { }
    }
}
