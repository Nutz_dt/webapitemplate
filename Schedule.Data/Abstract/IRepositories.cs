﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Scheduler.Model.Entities;

namespace Schedule.Data.Abstract
{
    public interface IScheduleRepository : IEntityBaseRepository<Scheduler.Model.Entities.Schedule> { }
 
	public interface IUserRepository : IEntityBaseRepository<User> { }
 
	public interface IAttendeeRepository : IEntityBaseRepository<Attendee> { }
}
